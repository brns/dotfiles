#!/bin/bash
# Installation
# :PROPERTIES:
# :header-args: :tangle ./install-xdesktop.sh :shebang #!/bin/bash :noweb yes :comments both
# :END:


# [[file:dwm.org::*Installation][Installation:1]]
if command -v pacman >/dev/null; then

    echo -e " \e[1m\e[92mInstall requirements for Arch Linux\e[21m..."
    sudo pacman -S dex sxhkd udiskie redshift picom feh xorg-xbacklight --needed

elif command -v apt >/dev/null; then
    echo -e " \e[1m\e[92mInstall requirements for Ubuntu\e[21m..."
    echo -e "Requirements for Ubuntu missing"
fi
# Installation:1 ends here
