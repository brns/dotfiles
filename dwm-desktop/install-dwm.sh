#!/bin/bash
# Main script


# [[file:dwm.org::*Main script][Main script:1]]
#install-dwm.sh
set -e

if command -v pacman >/dev/null; then

    echo -e " \e[1m\e[92m********************************************"
    echo -e " \e[1m\e[92mInstall requirements for Arch Linux\e[21m..."
    echo -e " \e[1m\e[92m********************************************"

   # Arch Linux
    sudo pacman -S dmenu rofi slock alacritty xorg-xsetroot dex volumeicon pasystray --needed
    

elif command -v apt >/dev/null; then

    echo -e " \e[1m\e[92mInstall requirements for Ubuntu\e[21m..."

   # Ubuntu
    #install-dwm.sh
    set -e
    
    if which apt-get &> /dev/null; then
        # Install rofi
        sudo apt-get install rofi libxft-dev libxinerama-dev
        if ! which dmenu &> /dev/null; then
    
            # Set it as dmenu replacement
            sudo ln -s $(which rofi) /usr/bin/dmenu
        fi
    fi
fi

# Change to this scripts parent directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

# create directory first, else stow creates them
[ -d ${HOME}/.local/bin ] || mkdir ${HOME}/.local/bin
[ -d ${HOME}/.config/autostart ] || mkdir ${HOME}/.config/autostart

stow -t $HOME  home
sudo rsync -av usr/ /usr/

# Clone and install my custom dwm fork
DIR=~/src/dwm
[ -d "${DIR}" ] || git clone git@gitlab.com:brns/dwm.git "${DIR}"
cd $DIR
git stash
git checkout brns
sudo make install
git stash pop
# Main script:1 ends here
