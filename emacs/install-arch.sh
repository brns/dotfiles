#!/bin/bash
# Hunspell

# https://github.com/munen/emacs.d#use-hunspell-with-multiple-dictionaries


# [[file:readme.org::*Hunspell][Hunspell:1]]
#  apt install hunspell hunspell-de-de hunspell-en-gb hunspell-en-us hunspell-de-ch-frami
sudo pacman -S hunspell hunspell-de hunspell-en_gb hunspell-en_us
# Hunspell:1 ends here
