;; Increase the GC threshold for faster startup
;; The default is 800 kilobytes.  Measured in bytes.
(setq gc-cons-threshold (* 50 1000 1000))

(defvar init.org-message-depth 3
  "What depth of init.org headings to message at startup.")


;; Using the function below has three main advantages to using org-babel-load-file.
;;   1. Explicit control over what blocks get evaluated.
;;   2. Messaging the exact heading containing a block causing an error.
;;   3. Not dependent on org-mode.
;; Inspired by [[http://endlessparentheses.com/init-org-Without-org-mode.html]["init.org Without org-mode"]]

(defun brns/eval-org-file (file-name)
  "Parse an org file and evaluates its emacs-lisp code blocks."
  (with-temp-buffer
    (insert-file-contents (expand-file-name file-name user-emacs-directory))

    (goto-char (point-min))

    ;; Skip straight to the first elisp code block.
    (re-search-forward "^[\s-]*#\\+begin_src +emacs-lisp$")
    ;; Set point to previous heading
    (re-search-backward (format "\\*\\{1,%s\\} +.*$"
                                init.org-message-depth))
    
    ;; ;; Alternatively, you can have all elisp code blocks under a single parent heading.
    ;; (search-forward "\n* init.el")


    ;; Code Blocks inside headlines with a
    ;; DISABLED keyword are not evaluated.
    (make-local-variable 'curr-hl-disabled)
    (setq curr-hl-disabled nil)
    
    
    ;; Begin parsing org file.
    (while (not (eobp))
      (forward-line 1)
      (cond
       ;; Report Headings
       ((looking-at
         (format "\\*\\{1,%s\\} +.*$"
                 init.org-message-depth))
	(let ((hl (match-string 0)))
	  (message "%s" hl)
	  (setq curr-hl-disabled (string-match-p (regexp-quote "DISABLED") hl))))
       ;; Messages where currently parsing.
       ;; Evaluate Code Blocks
       ((looking-at "^[\s-]*#\\+begin_src +emacs-lisp$")
        (let ((l (match-end 0)))
          (re-search-forward "^[\s-]*#\\+end_src$")
	  
          ;; Write evaluated elisp source blocks to a single file.
          (append-to-file l (match-beginning 0) "~/.emacs.d/initorg.el")
	  
          (if curr-hl-disabled (message "DISABLED") (eval-region l (match-beginning 0)))))
       ;; Finish on the next level-1 heading
       ((looking-at "^\\* ")
        (goto-char (point-max)))))
    ;; Startup message.
    (message "----- Loaded %s -----" file-name)))


(put 'set-goal-column 'disabled nil)


;; Define the following variables to remove the compile-log warnings
;; when defining ido-ubiquitous
;; (defvar ido-cur-item nil)
;; (defvar ido-default-item nil)
;; (defvar ido-cur-list nil)
;; (defvar predicate nil)
;; (defvar inherit-input-method nil)

;; Alternatively use org-mode to read the file
;;(require 'ob-tangle)
;;(org-babel-load-file (expand-file-name "readme.org" user-emacs-directory))

(progn (brns/eval-org-file "readme.org"))

;; (if (string-prefix-p  "stego" (shell-command-to-string "cat /etc/hostname"))
;;   (progn (brns/eval-org-file "main-29.org")
;;           (brns/eval-org-file "org-mode.org"))
;;   (progn (brns/eval-org-file "main-29.org")
;; 	 (brns/eval-org-file "email.org")
;; 	 (brns/eval-org-file "org-mode.org")))


(message "Don't Panic!")
;;(brns/eval-org-file "readme.org")


;; Make GC pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))
;; Set high for lsp-mode performance
;;(setq gc-cons-threshold 100000000)
;;(setq read-process-output-max (* 1024 1024)) ;; 1mb
