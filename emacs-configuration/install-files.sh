#!/bin/bash
# Change to this scripts parent directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

# create directory first, else stow creates them
[ -d ${HOME}/.local/share/applications ] || mkdir ${HOME}/.local/share/applications

stow -t $HOME  home

# enable desktop files
update-desktop-database ~/.local/share/applications/

#sudo rsync -av usr/ /usr/
