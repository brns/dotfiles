#+SETUPFILE: ../theme-readtheorg.setup

#  Disable super/subscripting
#+OPTIONS: ^:{}
#+OPTIONS: toc:2

#+PROPERTY: header-args :exports code :eval never-export  :noweb yes :mkdirp yes :comments both
#+STARTUP: overview
#+BABEL: :cache yes :noweb yes

* Tools

** Install

#+begin_src bash :noweb-ref install--files :tangle ./install.sh :shebang #!/bin/bash 
  #yay -S protonvpn-cli protonvpn-applet --needed

  yay -S proton-vpn-gtk-app protonvpn-cli-community

  sudo pacman -S --needed network-manager-applet

  # File Manager
  sudo pacman -S --needed thunar nemo
#+end_src

#+RESULTS:
| AUR | Explicit                | (2):               | proton-vpn-gtk-app-4.4.3-1, | protonvpn-cli-community-2.2.12-1 |                |           |                              |   |    |      |     |
| ::  | [1mPKGBUILD           | up                 | to                          | date,                            | skipping       | download: | protonvpn-cli-community[0m |   |    |      |     |
| ::  | [1m(1/1)              | Downloaded         | PKGBUILD:                   | proton-vpn-gtk-app[0m          |                |           |                              |   |    |      |     |
| 2   | protonvpn-cli-community | (Installed)        | (Build                      | Files                            | Exist)         |           |                              |   |    |      |     |
| 1   | proton-vpn-gtk-app      | (Installed)        | (Build                      | Files                            | Exist)         |           |                              |   |    |      |     |
| ==> | Packages                | to                 | cleanBuild?                 |                                  |                |           |                              |   |    |      |     |
| ==> | [N]one                  | [A]ll              | [Ab]ort                     | [I]nstalled                      | [No]tInstalled | or        | (1                           | 2 | 3, | 1-3, | ^4) |
| ==> | 2                       | proton-vpn-gtk-app | (Installed)                 | (Build                           | Files          | Exist)    |                              |   |    |      |     |
| 1   | protonvpn-cli-community | (Installed)        | (Build                      | Files                            | Exist)         |           |                              |   |    |      |     |
| ==> | Diffs                   | to                 | show?                       |                                  |                |           |                              |   |    |      |     |
| ==> | [N]one                  | [A]ll              | [Ab]ort                     | [I]nstalled                      | [No]tInstalled | or        | (1                           | 2 | 3, | 1-3, | ^4) |
| ==> |                         |                    |                             |                                  |                |           |                              |   |    |      |     |

** Install files
    
    #+name: install-files
    #+begin_src bash :noweb-ref install--files :tangle ./install-files.sh :shebang #!/bin/bash 
      # Change to this scripts parent directory
      DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
      cd $DIR

      # create directory first, else stow creates them
      [ -d ${HOME}/.local/bin ] || mkdir ${HOME}/.local/bin
      [ -d ${HOME}/.config/autostart ] || mkdir ${HOME}/.config/autostart
      [ -d ${HOME}/.config/mpd ] || mkdir ${HOME}/.config/mpd

      stow -t $HOME  home
      #sudo rsync -av usr/ /usr/
    #+end_src

** Proton VPN

*** Autostart

[[https://github.com/ProtonVPN/linux-app/issues/64][Github Issue]]

#+begin_src bash :shebang #!/bin/bash :tangle ./home/.local/bin/protonvpn-start.sh
  sleep 20

  # Kill switch
  protonvpn-cli ks --off
  protonvpn-cli ks --on

  # Netshield
  protonvpn-cli ns --off
  # or --malware if you don't want to block ads
  protonvpn-cli ns --ads-malware

  # Start VPN
  #protonvpn-cli c -f
  protonvpn c --sc 
#+end_src


#+begin_src bash :tangle ./home/.config/autostart/protonvpn-cli.desktop
    [Desktop Entry]
  Type=Application
  #Exec=/home/brns/.local/bin/protonvpn-start.sh
  Exec=/usr/bin/protonvpn-app
  Hidden=false
  NoDisplay=false
  X-GNOME-Autostart-enabled=true
  Name[en_GB]=proton VPN CLI
  Name=proton VPN CLI
  Comment[en_GB]=
  Comment=
#+end_src

** Music

*** MPD

#+begin_src conf :tangle ./home/.config/mpd/mpd.conf
  # Recommended location for database
   db_file            "~/.config/mpd/database"

   # If running mpd using systemd, delete this line to log directly to systemd.
   log_file           "syslog"

   # The music directory is by default the XDG directory, uncomment to amend and choose a different directory
   music_directory    "~/music"

   # Uncomment to refresh the database whenever files in the music_directory are changed
   auto_update "yes"

   # Uncomment to enable the functionalities
   playlist_directory "~/.config/mpd/playlists"
   pid_file           "~/.config/mpd/pid"
   state_file         "~/.local/state/mpd/state"
   sticker_file       "~/.config/mpd/sticker.sql"
#+end_src



* Privacy 

** Mullvad DNS

Refer to the [[https://mullvad.net/en/help/dns-over-https-and-dns-over-tls][guide on mullvad.net]] to verify the IPs.

#+begin_src bash
  sudo systemctl enable systemd-resolved
#+end_src
