#!/bin/bash
# Install files
    
#     #+name: install-files

# [[file:readme.org::install-files][install-files]]
# Change to this scripts parent directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

# create directory first, else stow creates them
[ -d ${HOME}/.local/bin ] || mkdir ${HOME}/.local/bin
[ -d ${HOME}/.config/autostart ] || mkdir ${HOME}/.config/autostart
[ -d ${HOME}/.config/mpd ] || mkdir ${HOME}/.config/mpd

stow -t $HOME  home
#sudo rsync -av usr/ /usr/
# install-files ends here
