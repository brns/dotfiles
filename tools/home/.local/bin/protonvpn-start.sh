#!/bin/bash
# Autostart

# [[https://github.com/ProtonVPN/linux-app/issues/64][Github Issue]]


# [[file:../../../readme.org::*Autostart][Autostart:1]]
sleep 20

# Kill switch
protonvpn-cli ks --off
protonvpn-cli ks --on

# Netshield
protonvpn-cli ns --off
# or --malware if you don't want to block ads
protonvpn-cli ns --ads-malware

# Start VPN
#protonvpn-cli c -f
protonvpn c --sc
# Autostart:1 ends here
