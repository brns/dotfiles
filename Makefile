

init-arch:
	sudo pacman -S --needed \
			emacs \
			stow  \
			git \
			rsync \
			bash-completion

init-emacs-arch:
	yay -S ttf-iosevka-nerd ttf-nerd-fonts-hack-complete-git --needed

	bash emacs/install-arch.sh	

install-emacs:
	stow -t ${HOME}/.emacs.d emacs


install-emacs-org-protocol:
	stow -t ${HOME} -d emacs-configuration home

install-tools:
	./tools/install-tools.sh


