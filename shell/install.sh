#!/bin/bash
# Install


# [[file:readme.org::*Install][Install:1]]
sudo pacman -S npm --needed
yay -S --needed nvm
# Install:1 ends here

# Set NPM global directory

# Allow installing global packages without sudo


# [[file:readme.org::*Set NPM global directory][Set NPM global directory:1]]
[ -d ${HOME}/.local/bin ] || mkdir $HOME/.npm-global
npm config set prefix '~/.npm-global'
# Set NPM global directory:1 ends here
