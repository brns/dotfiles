#!/bin/bash
# Install ZSH


# [[file:readme.org::*Install ZSH][Install ZSH:1]]
sudo pacman -S --needed zsh zsh-completions
# Install ZSH:1 ends here

# Install Antigen

# [[https://github.com/zsh-users/antigen][Antigen]] is a small plugin manager for ZSH.


# [[file:readme.org::*Install Antigen][Install Antigen:1]]
curl -L git.io/antigen > $HOME/.config/zsh/antigen.zsh
# Install Antigen:1 ends here

# Change Shell


# [[file:readme.org::*Change Shell][Change Shell:1]]
chsh -l

echo "Run 'chsh -s /path/to/shell' to set the default shell"
# Change Shell:1 ends here
