# =fish.conf=
# :PROPERTIES:
# :header-args: :tangle ./home/.config/fish/config.fish :noweb yes :comments both :mkdirp yes
# :END:


# [[file:../../../readme.org::*=fish.conf=][=fish.conf=:1]]
if status is-interactive
  # Commands to run in interactive sessions can go here
  bass source ~/.profile
end
# =fish.conf=:1 ends here
