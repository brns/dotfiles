#!/bin/bash
# Initial


# [[file:../readme.org::*Initial][Initial:1]]
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
PS1='[\u@\h \W]\$ '
# Initial:1 ends here

# Virtualenvwrapper

# For Python development


# [[file:../readme.org::*Virtualenvwrapper][Virtualenvwrapper:1]]
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/git
#source /usr/local/bin/virtualenvwrapper.sh
source /usr/bin/virtualenvwrapper.sh
# Virtualenvwrapper:1 ends here
