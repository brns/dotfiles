# Add npm-global to path


# [[file:../readme.org::*Add npm-global to path][Add npm-global to path:1]]
export PATH=~/.npm-global/bin:$PATH
# Add npm-global to path:1 ends here

# Set NVM variables


# [[file:../readme.org::*Set NVM variables][Set NVM variables:1]]
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
# Set NVM variables:1 ends here
