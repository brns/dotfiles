# Virtualenvwrapper

# For Python development


# [[file:../readme.org::*Virtualenvwrapper][Virtualenvwrapper:1]]
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/git
#source /usr/local/bin/virtualenvwrapper.sh
source /usr/bin/virtualenvwrapper.sh
# Virtualenvwrapper:1 ends here

# Antigen config

# Check [[https://github.com/unixorn/awesome-zsh-plugins][awesome-zsh-plugins]] for more.

# https://github.com/zthxxx/jovial


# [[file:../readme.org::*Antigen config][Antigen config:1]]
source $HOME/.config/zsh/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle heroku
antigen bundle pip
antigen bundle lein
antigen bundle command-not-found

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting

# Load the theme.
# antigen theme dracula/zsh
antigen theme robbyrussell
# antigen theme ArmandPhilippot/coldark-zsh-theme
#antigen theme spaceship-prompt/spaceship-prompt
#antigen bundle reobin/typewritten@main

# Tell Antigen that you're done.
antigen apply
# Antigen config:1 ends here
