#!/bin/bash
# Fish shell


# [[file:readme.org::*Fish shell][Fish shell:1]]
sudo pacman -S fish fisher

chsh -l

echo "Run 'chsh -s /usr/bin/fish' to set the default shell"
# Fish shell:1 ends here

# Plugins


# [[file:readme.org::*Plugins][Plugins:1]]
fish -c "fisher install jorgebucaran/nvm.fish"

pipx install virtualfish
fish -c "vf install compat_aliases projects environment"
fish -c "fisher install edc/bass"

poetry completions fish > ~/.config/fish/completions/poetry.fish
# Plugins:1 ends here

# Pure Theme

# Install the [[https://pure-fish.github.io/pure/#separate-error-symbol][pure-fish]] theme.


# [[file:readme.org::*Pure Theme][Pure Theme:1]]
fish -c "fisher install pure-fish/pure"
# Pure Theme:1 ends here
