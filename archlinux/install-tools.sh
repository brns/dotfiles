#!/bin/bash
# XFCE Config


# [[file:readme.org::*XFCE Config][XFCE Config:1]]
yay -S --needed nordic-theme
# XFCE Config:1 ends here

# Fonts


# [[file:readme.org::*Fonts][Fonts:1]]
sudo pacman -S --needed noto-fonts noto-fonts-emoji ttf-linux-libertine

yay -S ttf-iosevka-nerd
yay -S ttf-nerd-fonts-hack-complete-git

yay -S --needed ttf-hack-nerd ttf-anonymouspro-nerd ttf-gohu-nerd ttf-space-mono-nerd
# Fonts:1 ends here

# Power Management


# [[file:readme.org::*Power Management][Power Management:1]]
sudo pacman -S --needed powertop tlp
yay -S auto-cpufreq

systemctl enable tlp --now
sudo auto-cpufreq --install # installs daemon and gui
# Power Management:1 ends here

# Bluetooth


# [[file:readme.org::*Bluetooth][Bluetooth:1]]
sudo pacman -S bluez bluez-utils blueman --needed
systemctl enable bluetooth.service --now
# Bluetooth:1 ends here

# Desktop Tools


# [[file:readme.org::*Desktop Tools][Desktop Tools:1]]
sudo pacman -S --needed lxrandr lxappearance
yay -S font-manager
# Desktop Tools:1 ends here
