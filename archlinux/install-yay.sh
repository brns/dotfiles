!/bin/bash
# AUR Package Manager

# To access the Arch User Repository we install ~yay~.
# This has to be done manually as described on the [[https://github.com/Jguer/yay][Github page]].


# [[file:readme.org::*AUR Package Manager][AUR Package Manager:1]]
cd /tmp

sudo pacman -S --needed git base-devel

git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
# AUR Package Manager:1 ends here
